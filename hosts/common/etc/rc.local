#!/bin/sh -e
#
# rc.local
#
# This script is executed at the end of each multiuser runlevel.
# Make sure that the script will "exit 0" on success or any other
# value on error.

set -x

#
# /etc/rc.local runs at boot time, so we do some stuff and then we send *some*
# hosts back to the future for reproducibility testing. the others just run today.
#

send_back_to_the_future() {
	# disable systemd date services (and don't fail if systemd ain't running)
	systemctl disable systemd-timesyncd || true
	systemctl disable systemd-timedated || true
	systemctl disable ntp.service || true
	systemctl disable ntpsec.service || true
	systemctl stop systemd-timesyncd || true
	systemctl stop systemd-timedated || true
	systemctl stop ntp.service || true
	systemctl stop ntpsec.service || true
	# set correct date
	if [ -z "$1" ] ; then
		ntpdate -b de.pool.ntp.org
	else
		ntpdate -b "$1"
	fi
	# set fake date
	date --set="+398 days +6 hours + 23 minutes"
	# modify MESSAGE for notification
	MESSAGE="$MESSAGE in the future"
}

fixup_shm() {
	# this is always harmless
	chmod 1777 /dev/shm
}

fixup_pbuilder_lockfiles() {
	rm -f /var/cache/pbuilder/*.tgz.tmp
}

cleanup_srv_workspace() {
	rm -rf --one-file-system /srv/workspace/chroots/rbuild-openwrt-build-*
	rm -rf --one-file-system /srv/workspace/pbuilder/*
}

cleanup_rbuild_debian() {
	rm -rf --one-file-system /srv/reproducible-results/rbuild-debian/r-b-build.*
}

###
### main
###
MESSAGE="$(hostname -f) rebooted"

#
# fixup /(dev|run)/shm if needed
#
fixup_shm

#
# configure iptables to drop incoming UDP packets
#
iptables -I INPUT -p udp --dport 111 -j DROP

#
# clean up left-overs which make no sense after a reboot
#
fixup_pbuilder_lockfiles
cleanup_rbuild_debian
cleanup_srv_workspace

#
# notify jenkins reboots on irc
#
if [ "$(hostname)" = "jenkins" ] ; then
	for channel in debian-qa reproducible-builds ; do
		kgb-client --conf /srv/jenkins/kgb/$channel.conf --relay-msg "$MESSAGE"
	done
fi

#
# document what is "Debian testing" today (because distro-info doesn't like to be run in the future... :)
#
debian-distro-info --testing > /etc/debian_testing

#
# run some hosts in the future
#
case $(hostname) in
	codethink01*)		send_back_to_the_future ;;
	codethink03*)		send_back_to_the_future ;;
	ionos5*)		send_back_to_the_future ;;
	ionos6*)		send_back_to_the_future ;;
	ionos15*)		send_back_to_the_future ;;
	ionos16*)		send_back_to_the_future ;;
	osuosl2*)		send_back_to_the_future time.osuosl.org ;;
	infom02*)		send_back_to_the_future ch.pool.ntp.org ;;
	infom08*)		send_back_to_the_future ch.pool.ntp.org ;;
	*)			;;
esac

#
# notify about reboots
#
echo "$(date -u) - system was rebooted." | mail -s "$MESSAGE" root

exit 0
