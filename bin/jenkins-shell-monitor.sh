#!/bin/bash
# vim: set noexpandtab:

# Copyright © 2020-2024 Holger Levsen (holger@layer-acht.org)
# released under the GPLv2
#
#
# to be run manually on jenkins in a terminal
#
# this script is partly jelle's fault, because he gave me
# https://github.com/jelly/reproduciblebuilds-display
# which made me make https://tests.reproducible-builds.org/trbo.status.html
# first and then this!

# define some warning levels (in gigabytes) for some directories
declare -A WARNING_LEVEL
WARNING_LEVEL['/var/lib/jenkins']=80
WARNING_LEVEL['/var/lib/schroot']=70
WARNING_LEVEL['/srv/schroots']=100
WARNING_LEVEL['/srv/reproducible-results/rbuild-debian']=50
WARNING_LEVEL['r-b-userContent../dbd*/buster']=5
WARNING_LEVEL['r-b-userContent../dbd*/bullseye']=40
WARNING_LEVEL['r-b-userContent../dbd*/bookworm']=60
WARNING_LEVEL['r-b-userContent../dbd*/trixie']=60
WARNING_LEVEL['r-b-userContent../dbd*/unstable']=145
WARNING_LEVEL['r-b-userContent../dbd*/experimental']=42
WARNING_LEVEL['live_build']=23

# prepare a trap for cleaning up
# using a global tempfile
PSFAX=$(mktemp --tmpdir=$TMPDIR jenkins-shell-monitor-XXXXXXX)
DISKSTATS=$(mktemp --tmpdir=$TMPDIR jenkins-shell-monitor-XXXXXXX)
cleanup_all() {
	trap - INT TERM EXIT
	echo
	rm -vf $PSFAX $DISKSTATS
	echo "Terminated, good-bye."
	exit 0
}

#
# main
#
main_loop() {
	# most data is updated every 23 minutes in the outer loop,
	# except some, which is only updated every 180 minutes...:
	if [ $SEQ0 -eq 0 ] ; then
		#
		# gather directory stats
		#
		(
		printf  "%-45s %-5s %-5s %s\n" "mountpoint" "size" "avail" "usage"
		for FILESYSTEM in /dev/vda1 /var/lib/jenkins/userContent/reproducible /srv/workspace /tmp ; do
			DF_FILESYSTEM=$(df -h $FILESYSTEM | tail -1)
			FILESYSTEM_MOUNTPOINT=$(echo $DF_FILESYSTEM | awk '{ print $6 }')
			FILESYSTEM_SIZE=$(echo $DF_FILESYSTEM | awk '{ print $2 }')
			FILESYSTEM_AVAIL=$(echo $DF_FILESYSTEM | awk '{ print $4 }')
			FILESYSTEM_USAGE=$(echo $DF_FILESYSTEM | awk '{ print $5 }' | cut -d '%' -f1)
			if [ $FILESYSTEM_USAGE -gt 95 ] ; then
				FILESYSTEM_USAGE="${RED}$FILESYSTEM_USAGE%${FOREGROUND_COLOR}"
				FILESYSTEM_AVAIL="${RED}$FILESYSTEM_AVAIL${FOREGROUND_COLOR}"
				WIDTH=16
			elif [ $FILESYSTEM_USAGE -gt 90 ] ; then
				FILESYSTEM_USAGE="${ORANGE}$FILESYSTEM_USAGE%${FOREGROUND_COLOR}"
				FILESYSTEM_AVAIL="${ORANGE}$FILESYSTEM_AVAIL${FOREGROUND_COLOR}"
				WIDTH=16
			elif [ $FILESYSTEM_USAGE -gt 80 ] ; then
				FILESYSTEM_USAGE="${YELLOW}$FILESYSTEM_USAGE%${FOREGROUND_COLOR}"
				FILESYSTEM_AVAIL="${YELLOW}$FILESYSTEM_AVAIL${FOREGROUND_COLOR}"
				WIDTH=16
			else
				FILESYSTEM_USAGE="$FILESYSTEM_USAGE%"
				WIDTH=5
			fi
			printf  "%-45s %-5s %-${WIDTH}s %s\n" $FILESYSTEM_MOUNTPOINT $FILESYSTEM_SIZE $FILESYSTEM_AVAIL $FILESYSTEM_USAGE
			# include some directory stats which have been problematic in the past
			case $FILESYSTEM in
				"/dev/vda1")
					for DIR_PATH in /var/lib/jenkins /var/lib/schroot /srv/schroots /srv/reproducible-results/rbuild-debian ; do
						DU=$(sudo du -shx --apparent-size $DIR_PATH 2>/dev/null | awk '{ print $1 }')
						if [ -n "$(echo $DU | grep G)" ] && [ -n "${WARNING_LEVEL[$DIR_PATH]}" ] && [ "${WARNING_LEVEL[$DIR_PATH]}" -lt "$(echo $DU | cut -d 'G' -f1 | cut -d '.' -f1)" ] ; then
							DIR_USAGE="${YELLOW}$DU${FOREGROUND_COLOR}"
						else
							DIR_USAGE=$DU
						fi
						printf  "%-55s %-12s\n" "  $DIR_PATH" $DIR_USAGE
					done
					;;
				"/var/lib/jenkins/userContent/reproducible")
					DIR_PATH=/var/lib/jenkins/userContent/reproducible/debian
					for i in buster bullseye bookworm trixie unstable experimental live_build ; do
						if [ "$i" = "live_build" ] ; then
							DU_PATH="$i"
							VISIBLE_PATH="$i"
						else
							DU_PATH="dbd*/$i"
							VISIBLE_PATH="r-b-userContent../dbd*/$i"
						fi
						DU=$(cd $DIR_PATH ; du -ch --apparent-size ${DU_PATH} 2>/dev/null | grep total | awk '{ print $1 }')
						if [ -n "$(echo $DU | grep G)" ] && [ -n "${WARNING_LEVEL[$VISIBLE_PATH]}" ] && [ "${WARNING_LEVEL[$VISIBLE_PATH]}" -lt "$(echo $DU | cut -d 'G' -f1 | cut -d '.' -f1)" ] ; then
							DIR_USAGE="${YELLOW}$DU${FOREGROUND_COLOR}"
						else
							DIR_USAGE=$DU
						fi
						printf  "%-55s %-12s\n" "  $VISIBLE_PATH" $DIR_USAGE
					done
					;;
			esac
		done
		) > $DISKSTATS
		DISK_AGE="$(date -u)"
	fi
	ps fax > $PSFAX
	LSOF=$(lsof -n | wc -l)
	SCHROOT_SESSIONS=$(find /var/lib/schroot/session/ -mindepth 1 | wc -l)
	if [ $SCHROOT_SESSIONS -gt 30000 ] ; then
		SCHROOT_SESSIONS="${RED}$SCHROOT_SESSIONS${FOREGROUND_COLOR}"
	elif [ $SCHROOT_SESSIONS -gt 15000 ] ; then
		SCHROOT_SESSIONS="${YELLOW}$SCHROOT_SESSIONS${FOREGROUND_COLOR}"
	fi
	SCHROOT_MOUNTS=$(mount | grep /run/schroot| wc -l)
	SCHROOT_BASE_DIRS=$(find /schroots/ -maxdepth 1 -mindepth 1 -type d | wc -l)
	DIFFOSCOPE_PROCS=$(grep -c '/usr/bin/python3 /usr/bin/diffoscope' $PSFAX || true)
	REPRO_JOBS=$(ls ~jenkins/jobs/reproducible_* -1d | wc -l)
	JOBS_RUNNING_TOTAL=$(grep '_ /bin/bash /srv/jenkins/bin/' $PSFAX | grep -c -v -E 'reproducible_worker.sh|reproducible_build.sh|jenkins-shell-monitor.sh|reproducible_build_archlinux_pkg.sh')
	if [ $JOBS_RUNNING_TOTAL -eq 0 ] ; then
		JOBS_RUNNING_TOTAL="${RED}$JOBS_RUNNING_TOTAL${FOREGROUND_COLOR}"
	elif [ $JOBS_RUNNING_TOTAL -lt 5 ] ; then
		JOBS_RUNNING_TOTAL="${YELLOW}$JOBS_RUNNING_TOTAL${FOREGROUND_COLOR}"
	fi
	JOBS_RUNNING_LOCAL=$(grep '_ /bin/bash /srv/jenkins/bin/' $PSFAX | grep -c -v -E 'reproducible_worker.sh|reproducible_build.sh|jenkins-shell-monitor.sh|reproducible_build_archlinux_pkg.sh|jenkins_master_wrapper.sh')
	JOBS_RUNNING_REMOTE=$(grep -c jenkins_master_wrapper.sh $PSFAX || true)
	REPRO_WORKERS=$(grep -c reproducible_worker.sh $PSFAX || true)
	JENKINS_AGENTS=$(grep -c jenkins/agent.jar $PSFAX || true)
	NODES_TOTAL=$(find ~/nodes -mindepth 1 -type d | wc -l)
	SOME_AGE="$(date -u)"
	for i in $SEQ1 ; do
		# the inner loop is run more often
		clear
		if $(curl -s https://jenkins.debian.net/api/ |grep -q "Jenkins is going to shut down") ; then
			SHUTDOWN_MODE=true
		else
			SHUTDOWN_MODE=false
		fi
		LOAD="$(uptime | rev | cut -d ',' -f1-3 | rev | cut -d ':' -f2-|xargs echo)"
		INTEGER_LOAD=$(echo $LOAD | cut -d '.' -f1)
		# based on 23 CPUs
		if [ $INTEGER_LOAD -gt 69 ] ; then
			LOAD="${RED}$LOAD${FOREGROUND_COLOR}"
		elif [ $INTEGER_LOAD -gt 42 ] ; then
			LOAD="${YELLOW}$LOAD${FOREGROUND_COLOR}"
		fi
		NODES_OFFLINE="$(grep -v ^# ~jenkins/offline_nodes | grep -c debian)"
		NODES_GIT_OFFLINE="$(grep -v ^# ~jenkins-adm/jenkins.debian.net/jenkins-home/offline_nodes | grep -c debian)"
		if [ "$NODES_OFFLINE" != "$NODES_GIT_OFFLINE" ] ; then
			NODES_OFFLINE="${YELLOW}$NODES_OFFLINE${FOREGROUND_COLOR}"
		fi
		SYSTEMCTLSTATUS=$(systemctl status|head -4|tail -3)
		SYS_STATE=$(echo "$SYSTEMCTLSTATUS" | grep State | cut -d ':' -f2- | xargs echo)
		if [ "$SYS_STATE" != "running" ] ; then
			SYS_STATE="${YELLOW}$SYS_STATE${FOREGROUND_COLOR}"
		fi
		SYS_JOBS=$(echo "$SYSTEMCTLSTATUS" | grep Jobs | cut -d ':' -f2- | xargs echo)
		if [ "$SYS_JOBS" != "0 queued" ] ; then
			SYS_JOBS="${YELLOW}$SYS_JOBS${FOREGROUND_COLOR}"
		fi
		SYS_FAILED=$(echo "$SYSTEMCTLSTATUS" | grep Failed | cut -d ':' -f2- | xargs echo)
		if [ "$SYS_FAILED" != "0 units" ] ; then
			SYS_FAILED="${YELLOW}$SYS_FAILED${FOREGROUND_COLOR}"
		fi
		echo "=== jenkins/trbo shell monitor ==="
		echo
		OFFLINE_NODES=$(grep -A 2342 Also ~jenkins/offline_nodes | sed '/^$/d' | sed  "/^#/! s/^/$YELLOW/g" | sed  "/^#/! s/$/$FOREGROUND_COLOR/g" | grep -v "Down here nodes" | grep -v "failing their health check")
		# ^^^ this line explained:
		# we grep for everything after the string 'Also" because we know the document and this will never change... m( :)
		# the first sed command removes empty lines
		# the second sed command replaces the beginning of non-matching lines with $YELLOW
		# the third sed puts back the original foreground color..
		# and then we filter out some lines.
		if [ $(echo -e "$OFFLINE_NODES" | wc -l) -gt 1 ] ; then
			echo "relevant lines from ~jenkins/offline_nodes:"
			echo
			echo -e "$OFFLINE_NODES"
			echo
		fi
		STATUS=$(cat ~/userContent/reproducible/trbo.status || 0)
		if [ $STATUS -gt 239 ] ; then
			echo -e "${GREEN}$STATUS = trbo status${FOREGROUND_COLOR}, see https://tests.reproducible-builds.org/trbo.status.html"
		else
			if [ $STATUS -gt 223 ] ; then
				echo
			elif [ $STATUS -gt 207 ] ; then
				echo -e -n  "$YELLOW"
			elif [ $STATUS -gt 191 ] ; then
				echo -e -n  "$ORANGE"
			else
				echo -e -n  "$RED"
			fi
			figlet $STATUS
			echo -e -n "${FOREGROUND_COLOR}"
			echo " ^^^ trbo status, see https://tests.reproducible-builds.org/trbo.status.html"
		fi
		if $SHUTDOWN_MODE ; then
			echo
			echo -e "   ${YELLOW}FYI: jenkins is in SHUTDOWN mode.${FOREGROUND_COLOR}"
		fi
		echo
		echo    "uptime:                                       $(uptime -p | cut -d ' ' -f2-)"
		echo -e "load averages:                                $LOAD"
		echo -e "systemctl status summary:                     $SYS_STATE ($SYS_JOBS / $SYS_FAILED failed )"
		echo    "total number of user sessions / users:        $(uptime | rev |cut -d ',' -f1-4 | rev | cut -d ',' -f1 | sed "s#users##" | xargs echo) / $(w -h | awk '{print $1}' | sort -u | xargs echo)"
		echo    "number of open files:                         $LSOF"
		echo -e "schroot: (sessions / mounts / base dirs)      $SCHROOT_SESSIONS / $SCHROOT_MOUNTS / $SCHROOT_BASE_DIRS"
		echo    "configured r-b jobs:                          $REPRO_JOBS"
		echo -e "running jenkins jobs: (total/local/remote)    $JOBS_RUNNING_TOTAL / $JOBS_RUNNING_LOCAL / $JOBS_RUNNING_REMOTE"
		echo    "running r-b workers / diffoscope processes:   $REPRO_WORKERS / $DIFFOSCOPE_PROCS"
		echo    "running jenkings agents/nodes:                $JENKINS_AGENTS"
		echo -e "nodes: (total/auto-offline/offline in git)    $NODES_TOTAL / $NODES_OFFLINE / $NODES_GIT_OFFLINE"
		echo
		cat $DISKSTATS
		free -h | cut -b1-47
		echo
		echo "directory stats were gathered at $DISK_AGE. (updated every 180min)"
		echo "some information was gathered at $SOME_AGE. (updated every  23min)"
		echo "some other information at around $(date -u). (updated every  90sec)"
		for j in $SEQ2 ; do
			echo -n "..."
			sleep 4.5	# -> this involves loop runtime... (see below)
		done
	done
}

# main init
export LANG=C
trap cleanup_all INT TERM EXIT
# initialize internal loop variables
SEQ2=$(seq 1 20)   			# -> inner loop takes 90sec because 20*4.5s=90s
SEQ1=$(seq 1 15)   			# -> outer loop takes ~23min because 15*90s=23m
SEQ0=0
# static colors
FOREGROUND_COLOR=$(tput sgr0)
RED=$(tput setaf 1)
ORANGE=$(tput setaf 166)
YELLOW=$(tput setaf 3)
GREEN=$(tput setaf 2)
# main loop
while true ; do
	main_loop
	let SEQ0+=1
	if [ $SEQ0 -eq 8 ] ; then	# -> diskstats are only gathered every 180m
		SEQ0=0
	fi
done
