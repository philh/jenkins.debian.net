#!/usr/bin/python3

# Copyright 2019-2020 Holger Levsen <holger@layer-acht.org>
# Copyright 2024 Mattia Rizzolo <mattia@debian.org>
# released under the GPLv2

#
# This script helps powercycling x86 nodes at IONOS (previously Profitbricks).
#
# It needs an account and expects the environment variables
# IONOS_USERNAME and IONOS_PASSWORD to be set accordingly
# or have a token stored in ~/.config/ionos_token
#
# Then it expects node numbers or 'jenkins' as params.
#

import os
import sys
import logging

import ionoscloud


log = logging.getLogger(__name__)
_ch = logging.StreamHandler()
_ch.setFormatter(logging.Formatter(("%(levelname)s: %(message)s")))
log.addHandler(_ch)
log.setLevel(logging.INFO)
# log.setLevel(logging.DEBUG)

nodes = []
for argument in sys.argv[1:]:
    try:
        node = int(argument)
    except ValueError:
        # argument wasn't an integer
        if argument == "jenkins":
            nodes.append(argument)
        else:
            log.error("Unrecognized node: %s", argument)
            print("This script helps powercycling x86 nodes at IONOS.")

            print()
            print("It needs an account and expects the environment variables")
            print("IONOS_USERNAME and IONOS_PASSWORD to be set accordingly.")
            print()
            print("Alternatively, you can save a token in a file named")
            print("~/.config/ionos_token.  Be mindful of security")
            print()
            print("Then it expects node numbers or 'jenkins' as params.")
            sys.exit(1)
    else:
        name = "build" + str(node)
        nodes.append(name)

log.debug("Acting on nodes: %s", nodes)

try:
    with open(os.path.expanduser("~/.config/ionos_token")) as f:
        configuration = ionoscloud.Configuration(token=f.read().strip())
except FileNotFoundError:
    configuration = ionoscloud.Configuration(
        username=os.getenv("IONOS_USERNAME"),
        password=os.getenv("IONOS_PASSWORD")
    )
client = ionoscloud.ApiClient(configuration)

dcs = ionoscloud.DataCentersApi(client).datacenters_get()
log.debug(dcs)

for dc in dcs.items:
    datacenter = dc.id
    log.debug(datacenter)
    servers = ionoscloud.ServersApi(client).datacenters_servers_get(
        datacenter_id=datacenter, depth=1
    )
    log.debug(servers)
    for server in servers.items:
        log.debug(server.id)
        log.debug(server.type)
        server_name = server.properties.name
        log.debug(server_name)
        for node in nodes:
            if server.type == "server" and server_name == node:
                log.info(f"Rebooting {server_name} aka {node}")
                result = ionoscloud.ServersApi(client).\
                    datacenters_servers_reboot_post(
                         datacenter_id=datacenter, server_id=server.id)
                log.info(result)
